# Private things that should not be commited.
# Look at priv-example.

export GIT_AUTHOR_NAME='Fernando Basso'
export GIT_COMMITTER_NAME="$GIT_AUTHOR_NAME"
export GIT_AUTHOR_EMAIL='fernandobasso.br@gmail.com'
export GIT_COMMITTER_EMAIL="$GIT_AUTHOR_EMAIL"

# vim: set filetype=sh:
#
